//
//  RecipeDetailViewController.h
//  ObjCRecipeAssignment
//
//  Created by Douglas Sass on 2/8/17.
//  Copyright © 2017 Douglas Sass. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RecipeDetailViewController : UIViewController

@property (strong, nonatomic) IBOutlet UILabel *recipeTitleLbl;
@property (strong, nonatomic) IBOutlet UIImageView *recipeImageView;
@property (strong, nonatomic) IBOutlet UITextView *recipeIngredientsTextView;
@property (strong, nonatomic) IBOutlet UITextView *recipeInstructionsTextView;

@property (strong, nonatomic) NSDictionary* recipesInformation;

@end
