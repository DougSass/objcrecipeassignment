//
//  ViewController.m
//  ObjCRecipeAssignment
//
//  Created by Douglas Sass on 2/8/17.
//  Copyright © 2017 Douglas Sass. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    self.title = @"Recipes";
    
    recipesArray = [NSArray arrayWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"RecipePList" ofType:@"plist"]];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return recipesArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (!cell) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    
    NSDictionary* d = recipesArray[indexPath.row];
    
    cell.textLabel.text = d[@"recipeTitle"];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [self performSegueWithIdentifier:@"recipeSegue" sender:indexPath];
}


-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    NSIndexPath* ip = (NSIndexPath*)sender;
    if ([segue.identifier isEqualToString:@"recipeSegue"]) {
        RecipeDetailViewController* rdvc = (RecipeDetailViewController*)segue.destinationViewController;
        rdvc.recipesInformation = recipesArray[ip.row];
    }
}





//-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
//    return recipesArray.count;
//}
//
//-(UITableViewCell *)tableView:(UITableView *) tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
//    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
//    if (!cell) {
//        
//        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
//    }
//    
//    NSDictionary* d = recipesArray[indexPath.row];
//    
//    cell.textLabel.text = d[@"recipeTitle"];
//    
//    return cell;
//    
//}
//
//-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
//    
//    [tableView deselectRowAtIndexPath:indexPath animated:YES];
//    [self performSegueWithIdentifier:@"recipeSegue" sender:indexPath];
//}
//
//-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
//    NSIndexPath* ip = (NSIndexPath*)sender;
//    if ([segue.identifier isEqualToString:@"recipeSegue"]) {
//        RecipeDetailViewController* rdvc = (RecipeDetailViewController*)segue.destinationViewController;
//        rdvc.recipesInformation = recipesArray[ip.row];
//    }
//}


@end
