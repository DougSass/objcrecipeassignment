//
//  main.m
//  ObjCRecipeAssignment
//
//  Created by Douglas Sass on 2/8/17.
//  Copyright © 2017 Douglas Sass. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
