//
//  RecipeDetailViewController.m
//  ObjCRecipeAssignment
//
//  Created by Douglas Sass on 2/8/17.
//  Copyright © 2017 Douglas Sass. All rights reserved.
//

#import "RecipeDetailViewController.h"

@interface RecipeDetailViewController ()

@end

@implementation RecipeDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    self.recipeTitleLbl.text = self.recipesInformation[@"recipeTitle"];
    self.recipeIngredientsTextView.text = self.recipesInformation[@"recipeIngredients"];
    self.recipeInstructionsTextView.text = self.recipesInformation[@"recipeInstructions"];
    
    NSData * imageData = [[NSData alloc] initWithContentsOfURL: [NSURL URLWithString: self.recipesInformation[@"recipeImage"]]];
    self.recipeImageView.image = [UIImage imageWithData: imageData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
